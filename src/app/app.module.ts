import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms'; 

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';

import { RouterModule } from '@angular/router';
import { UsersService } from './users/users.service';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { UserFormComponent } from './users/user-form/user-form.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';
import { UsersFirebaseComponent } from './users/users-firebase/users-firebase.component';
import { LoginComponent } from './login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    NavigationComponent,
    UsersFormComponent,
    UserFormComponent,
    UsersFirebaseComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
     {path: '', component: ProductsComponent},
     {path: 'users', component: UsersComponent},
     {path: 'user-form/:id', component: UserFormComponent},
     {path: 'users-firebase', component: UsersFirebaseComponent},
     {path: 'login', component: LoginComponent},
     {path: '**', component: NotFoundComponent}
      ])
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
