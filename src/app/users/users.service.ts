import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from './../../environments/environment';

@Injectable()
export class UsersService {
  http:Http;
  getUsers(){
    //get messages from the SLIM rest API (Don't say DB)
    //return  this.http.get('http://localhost/slimexampletest/users');
    let token='?token='+localStorage.getItem('token');
    return  this.http.get(environment.url +'users' + token);    }
  
  postUsers(data){
    let token = localStorage.getItem('token');
    let params = new HttpParams().append('name',data.name).append('phone',data.phone).append('token',token); ;      
    //המרת גייסון למפתח וערך
    let options = {
    headers:new Headers({
    'content-type':'application/x-www-form-urlencoded'
    })
    }
    //return this.http.post('http://localhost/slimexampletest/users', params.toString(), options);
    return  this.http.post(environment.url +'users', params.toString(), options);

    }

    deleteUser(key){
      //return this.http.delete('http://localhost/slimexampletest/users/'+ key);
      return this.http.delete(environment.url +'users/'+ key);
    }
    
  getUser(key){  
    //return this.http.get('http://localhost/slimexampletest/users/'+key);
    return this.http.get(environment.url +'users/'+ key);

 }

 updateUser(data){       
  let options = {
    headers: new Headers({
      'content-type':'application/x-www-form-urlencoded'
    })   
  };


  let params = new HttpParams().append('name',data.name).append('phone',data.phone);  

 // return this.http.put('http://localhost/slimexampletest/users/'+ data.id,params.toString(), options);      
 return this.http.put(environment.url +'users/'+ data.id,params.toString(), options); 
}

    //השרת שמתחבר לפיירבייס
    getUserFire(){
      //הוויליו מייצר את האובזווריבל
      return this.db.list('/users').valueChanges();
     }
       //Login Witout JWT
  
    login(credentials){
     let options = {
        headers:new Headers({
         'content-type':'application/x-www-form-urlencoded'
        })
     }
    let  params = new HttpParams().append('user', credentials.user).append('password',credentials.password);
    return this.http.post('http://localhost/slimexampletest/auth', params.toString(),options).map(response=>{ 
      //let success = response.json().success;
      //if (success == true){
        //localStorage.setItem('auth','true');
      //}else{
        //localStorage.setItem('auth','false');        
      //}
      let token = response.json().token;
      if(token) localStorage.setItem('token',token);

   });
  }
  
    constructor(http:Http,private db:AngularFireDatabase) { 
      this.http = http;}

}
