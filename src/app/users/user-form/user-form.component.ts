import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router,ParamMap, Routes  } from '@angular/router';
import { Http,Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { UsersService } from '../users.service';


@Component({
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  name;
  phone;
 
  // Adding emitters
  @Output() addUser:EventEmitter <any> = new EventEmitter <any>();
  @Output() addUserPs:EventEmitter <any> = new EventEmitter <any>();
  // Instance Variables
  id;
  user;
  service:UsersService;
  //Form Builder
  usrform = new FormGroup({
      name:new FormControl(),
      phone:new FormControl(),
      id:new FormControl()
  });  

  constructor(service:UsersService, private formBuilder:FormBuilder, private route: ActivatedRoute, private router: Router) {   	    
    this.service = service;    
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = +params.get('id'); // This line converts id from string into num      
      this.service.getUser(this.id).subscribe(response=>{
        this.user = response.json(); 
        console.log(this.user.name);
        this.name = this.user.name
        this.phone = this.user.phone                                 
      });      
    });
  }

  sendData(){
    this.addUser.emit(this.usrform.value.name);
    this.usrform.value.id = this.id;
    
    this.service.updateUser(this.usrform.value).subscribe(
      response =>{              
        this.addUserPs.emit();
        this.router.navigate(['/users']);

      }

    )
  }
  ngOnInit() {/*
    //Login without JWT
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
     // this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    } */ 
  }

}
