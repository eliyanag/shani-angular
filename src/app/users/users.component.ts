import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { Router } from "@angular/router"; //Login without JWT

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  invalid = false;

  users;
  usersKeys;

  constructor(private service:UsersService, private router:Router) {
    service.getUsers().subscribe(response=>{
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
    });
   }
   deleteUser(key){
    console.log.apply(key);
    let index = this.usersKeys.indexOf(key);
    this.usersKeys.splice(index,1);
    //delete from server
    this.service.deleteUser(key).subscribe(
      response=>console.log(response)
    );
  }
    updateUser(id){
        this.service.getUser(id).subscribe(response=>{
          this.users = response.json();      
      }); 
    }  
    optimisticAdd(name)
   {
    var newKey = this.usersKeys[this.usersKeys.length-1]+1;
    console.log(this.usersKeys.length);
    console.log(this.usersKeys);
    var newUserObject = {};
    newUserObject['name'] = name;
    console.log(newUserObject);
    this.users[newKey] = newUserObject;
    this.usersKeys = Object.keys(this.users);
    console.log(this.users);
    console.log(event,newKey);
  }
  pessimiaticAdd()
  {
    this.service.getUsers().subscribe(response => {
      this.users =  response.json();
      this.usersKeys = Object.keys(this.users);
    });   
  }
    //Login without JWT
    logout(){
      localStorage.removeItem('token');
      this.invalid = false;
     }
  ngOnInit() {
        //Login without JWT
       /* var value = localStorage.getItem('auth');
    
        if(value == 'true'){   
          this.router.navigate(['/']);
        }else{
          this.router.navigate(['/login']);
        }  
  }*/
  }

}
